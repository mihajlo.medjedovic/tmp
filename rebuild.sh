#!/usr/bin/env bash

cd /home/mihajlo/external-projects/allan/datacontroller/client
npm run build
cd /home/mihajlo/external-projects/allan/datacontroller/sas
sasjs c -t sas9
rm -rf sasjsbuild/tests
sasjs b -t sas9
cp sasjsbuild/mysas9deploy.sas /home/mihajlo/external-projects/allan/4gl/tmp/runme.sas
cd /home/mihajlo/external-projects/allan/4gl/tmp
git add .
git commit -m "update"
git push
