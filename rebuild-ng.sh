#!/usr/bin/env bash

cd /home/mihajlo/external-projects/allan/sasjs/angular-seed-app
npm run build
cd sasjs
sasjs c -t sas9
rm -rf sasjsbuild/tests
sasjs b -t sas9
cp ../sasjsbuild/sas9.sas /home/mihajlo/external-projects/allan/4gl/tmp/runme.sas
cd /home/mihajlo/external-projects/allan/4gl/tmp
git add .
git commit -m "update"
git push
